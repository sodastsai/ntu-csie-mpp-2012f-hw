# -*- encoding: utf-8 -*-

city_list = [
    {
        "CityID": 1,
        "City": "秋葉原"
    },
    {
        "CityID": 2,
        "City": "奧克蘭"
    },
    {
        "CityID": 3,
        "City": "巴塞隆納"
    },
    {
        "CityID": 4,
        "City": "北京"
    },
    {
        "CityID": 5,
        "City": "柏林"
    },
    {
        "CityID": 6,
        "City": "伯明罕"
    },
    {
        "CityID": 7,
        "City": "波士頓"
    },
    {
        "CityID": 8,
        "City": "布宜諾斯艾利斯"
    },
    {
        "CityID": 9,
        "City": "坎培拉"
    },
    {
        "CityID": 10,
        "City": "卡迪夫"
    },
    {
        "CityID": 11,
        "City": "千葉"
    },
    {
        "CityID": 12,
        "City": "芝加哥"
    },
    {
        "CityID": 13,
        "City": "重慶 "
    },
    {
        "CityID": 14,
        "City": "科隆"
    },
    {
        "CityID": 15,
        "City": "庫比蒂諾"
    },
    {
        "CityID": 16,
        "City": "愛丁堡"
    },
    {
        "CityID": 17,
        "City": "佛羅倫斯"
    },
    {
        "CityID": 18,
        "City": "法蘭克福"
    },
    {
        "CityID": 19,
        "City": "銀座"
    },
    {
        "CityID": 20,
        "City": "廣州"
    },
    {
        "CityID": 21,
        "City": "漢堡"
    },
    {
        "CityID": 22,
        "City": "香港"
    },
    {
        "CityID": 23,
        "City": "休士頓"
    },
    {
        "CityID": 24,
        "City": "花蓮"
    },
    {
        "CityID": 25,
        "City": "池袋"
    },
    {
        "CityID": 26,
        "City": "高雄"
    },
    {
        "CityID": 27,
        "City": "神戶"
    },
    {
        "CityID": 28,
        "City": "吉隆坡"
    },
    {
        "CityID": 29,
        "City": "京都"
    },
    {
        "CityID": 30,
        "City": "利茲"
    },
    {
        "CityID": 31,
        "City": "利物浦"
    },
    {
        "CityID": 32,
        "City": "倫敦"
    },
    {
        "CityID": 33,
        "City": "洛杉磯"
    },
    {
        "CityID": 34,
        "City": "里昂"
    },
    {
        "CityID": 35,
        "City": "馬德里"
    },
    {
        "CityID": 36,
        "City": "曼徹斯特"
    },
    {
        "CityID": 37,
        "City": "馬賽"
    },
    {
        "CityID": 38,
        "City": "目黑"
    },
    {
        "CityID": 39,
        "City": "邁阿密"
    },
    {
        "CityID": 40,
        "City": "米蘭"
    },
    {
        "CityID": 41,
        "City": "蒙特婁"
    },
    {
        "CityID": 42,
        "City": "山景城"
    },
    {
        "CityID": 43,
        "City": "慕尼黑"
    },
    {
        "CityID": 44,
        "City": "名古屋"
    },
    {
        "CityID": 45,
        "City": "南京"
    },
    {
        "CityID": 46,
        "City": "成田"
    },
    {
        "CityID": 47,
        "City": "紐約"
    },
    {
        "CityID": 48,
        "City": "大阪"
    },
    {
        "CityID": 49,
        "City": "牛津"
    },
    {
        "CityID": 50,
        "City": "帕羅奧圖"
    },
    {
        "CityID": 51,
        "City": "巴黎"
    },
    {
        "CityID": 52,
        "City": "費城"
    },
    {
        "CityID": 53,
        "City": "里約熱內盧"
    },
    {
        "CityID": 54,
        "City": "羅馬"
    },
    {
        "CityID": 55,
        "City": "薩爾瓦多"
    },
    {
        "CityID": 56,
        "City": "舊金山"
    },
    {
        "CityID": 57,
        "City": "聖保羅"
    },
    {
        "CityID": 58,
        "City": "札幌"
    },
    {
        "CityID": 59,
        "City": "西雅圖"
    },
    {
        "CityID": 60,
        "City": "上海"
    },
    {
        "CityID": 61,
        "City": "瀋陽"
    },
    {
        "CityID": 62,
        "City": "涉谷"
    },
    {
        "CityID": 63,
        "City": "品川"
    },
    {
        "CityID": 64,
        "City": "新宿"
    },
    {
        "CityID": 65,
        "City": "新加坡"
    },
    {
        "CityID": 66,
        "City": "墨田"
    },
    {
        "CityID": 67,
        "City": "雪梨"
    },
    {
        "CityID": 68,
        "City": "台中"
    },
    {
        "CityID": 69,
        "City": "台南"
    },
    {
        "CityID": 70,
        "City": "台北"
    },
    {
        "CityID": 71,
        "City": "天津"
    },
    {
        "CityID": 72,
        "City": "東京"
    },
    {
        "CityID": 73,
        "City": "多倫多"
    },
    {
        "CityID": 74,
        "City": "上野"
    },
    {
        "CityID": 75,
        "City": "溫哥華"
    },
    {
        "CityID": 76,
        "City": "維多利亞"
    },
    {
        "CityID": 77,
        "City": "華盛頓"
    },
    {
        "CityID": 78,
        "City": "威靈頓"
    },
    {
        "CityID": 79,
        "City": "西敏寺"
    },
    {
        "CityID": 80,
        "City": "武漢"
    },
    {
        "CityID": 81,
        "City": "橫濱"
    }
]
