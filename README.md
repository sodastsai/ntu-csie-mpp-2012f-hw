NTU CSIE Mobile Phone Programming 2012 Fall - iOS
====================================================

HW Data
----------------------------------------------------
- data_soure_ios: data to be used in iOS project
  `(Extended from previous homework. Add coordinate)`
- images: image to be used in iOS project
  `(The same as previous homework)`
  `Download via link in following section`
- Other files: server files. `(Based on flask and heroku)`

Images
----------------------------------------------------
- Get images from here.
`https://s3-ap-northeast-1.amazonaws.com/ntumpp.sodas.tw/files/HW3-images.tar.bz2`

