import httplib
import json
import os
from flask import Flask, render_template, make_response
from data import city_list

app = Flask(__name__)

@app.route('/')
def root():
    return render_template('docs.html')


@app.route('/api/city/<city_id>/')
def city(city_id):
    try:
        city = [city for city in city_list if city['CityID']==int(city_id)][0]
        response = make_response(json.dumps(city))
    except ValueError:
        error = {
            'error': '"%s" is not a valid city id.'%city_id,
            'status': httplib.BAD_REQUEST,
            'reason': httplib.responses[httplib.BAD_REQUEST],
            }
        response = make_response(json.dumps(error), httplib.BAD_REQUEST)
    except IndexError:
        error = {
            'error': 'CityID=%s doesn\'t exists.'%city_id,
            'status': httplib.NOT_FOUND,
            'reason': httplib.responses[httplib.NOT_FOUND],
        }
        response = make_response(json.dumps(error), httplib.NOT_FOUND)

    response.headers['Content-Type'] = 'application/json'
    return response


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.debug = bool(os.environ.get('DEBUG', ''))
    app.run(host='0.0.0.0', port=port)
